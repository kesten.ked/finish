﻿using datedocker.Models;
using Microsoft.EntityFrameworkCore;

namespace SqlEfApi.Data
{
    public class EFContext : DbContext
    {
        public EFContext(DbContextOptions dbContextOptions) : base(dbContextOptions) { }

        public DbSet<Lesson3> Lesson3 { get; set; }

    }
}
