﻿namespace SqlEfApi.Logging
{
    // реализует класс ILogger
    public class DbLogger : ILogger
    {
        private readonly string _loggerName;
        private readonly DbLoggerConfiguration _config;

        public DbLogger(string loggername, DbLoggerConfiguration config)
        {
            _loggerName = loggername;
            _config = config;
        }
        
        
        public IDisposable BeginScope<TState>(TState state) => default;
       
        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(
            LogLevel logLevel,
            EventId eventId,
            TState state,
            Exception exception,
            Func<TState, Exception, string> formatter)
        {
            Console.WriteLine(formatter(state, exception));
            
            
            string fileName = Path.Combine(Environment.CurrentDirectory, "logger.txt");

            //Создаем пустой файл
            StreamWriter swBegin = new StreamWriter(fileName);
            swBegin.WriteLine("");
            swBegin.Close();

            //Дозаписываем файл
            StreamWriter sw = new StreamWriter(fileName, true); ;
            sw.WriteLine(formatter(state, exception));
            sw.Close();

        }
    }
}
