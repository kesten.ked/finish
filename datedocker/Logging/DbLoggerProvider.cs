﻿using Microsoft.Extensions.Options;
using System.Collections.Concurrent;

namespace SqlEfApi.Logging
{
    // связь с alias в appsetting
    [ProviderAlias("DbLogging")]
    public class DbLoggerProvider : ILoggerProvider
    {
        private readonly ConcurrentDictionary<string, DbLogger> _loggers = new();
        private readonly DbLoggerConfiguration _config;

        // конструтор. Выделяет в памяти место под наш объект
        public DbLoggerProvider(IOptionsMonitor<DbLoggerConfiguration> config) => _config = config.CurrentValue;

        public ILogger CreateLogger(string categoryName) => 
            _loggers.GetOrAdd(categoryName, name => new DbLogger(name, _config));

        public void Dispose() => _loggers.Clear();
    }
}
