terraform {
  required_version = ">= 0.13"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.47.0"
    }

  }
}

provider "yandex" {
  token     = "AQAAAAAjEia2AATuwRuG_9ouwUz7viAzKK03Pr0"
  cloud_id  = "b1gaq7sfo7btg6qur1d1"
  folder_id = "b1gachagpur6qqka5n5i"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "terra2" {
  name                      = "terra2"
  platform_id               = "standard-v1"
  zone                      = "ru-central1-a"
  hostname                  = "yandex"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    memory        = 2
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ciuqfa001h8s9sa7i"
      size     = 20
      type     = "network-hdd"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.devops.id
    nat       = true
  }
  scheduling_policy {
    preemptible = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_vpc_network" "terra22" {
  name = "terra22"
}

resource "yandex_vpc_subnet" "devops" {
  v4_cidr_blocks = ["10.131.0.0/24"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.terra22.id
}

output "internal_ip_address_terra2" {
  value = yandex_compute_instance.terra2.network_interface.0.ip_address
}
output "external_ip_address_terra2" {
  value = yandex_compute_instance.terra2.network_interface.0.nat_ip_address
}
